package main

import (
	"fmt"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	//"errors"
	"log"
	"math/rand"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly/v2"
)

const (
	ListingURL   string = "https://www.avaloncommunities.com/massachusetts/lexington-apartments/avalon-at-lexington"
	TwoBeds      string = "/apartments?bedroom=2BD"
	TransformSet string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

func Random() string {
	// make a string of bytes, and return N of them
	var num = 10

	b := make([]byte, rand.Intn(num)+num)
	for i := range b {
		b[i] = TransformSet[rand.Intn(len(TransformSet))]
	}
	return string(b)
}

type Floorplan struct {
	Bedrooms   int
	Bathrooms  int
	Squarefeet int
	Loft       bool
	ImageLink  string
}

type Apartment struct {
	Link         string
	Finish       string
	Unit         string
	Price        string
	LeaseTerm    int
	Availability string
	Floorplan    Floorplan
}

func ParseApartment(elem *colly.HTMLElement) (Apartment, error) {
	var apt Apartment
	var err error

	aptLink, exists := elem.DOM.Find("a").Attr("href")
	if exists {
		apt.Link = fmt.Sprintf("%s/%s", ListingURL, aptLink)
	}

	floorplanLink, _ := elem.DOM.Find("a > ul > li.carousel-item > img").Attr("data-src")
	u, err := url.Parse(floorplanLink)
	if err != nil {
		panic(err)
	}

	floorplanImage := fmt.Sprintf("%s://%s%s", u.Scheme, u.Host, u.Path)

	elem.DOM.Find("div.content > div").Each(func(i int, selection *goquery.Selection) {
		var text = selection.Text()
		var classes, exists = selection.Attr("class")

		var classSplit = strings.Split(classes, " ")

		if exists {
			switch strings.TrimSpace(classSplit[0]) {
			case "tag":
				// finish package I
				apt.Finish = text
			case "title":
				// apartment 007-7322
				apt.Unit = strings.SplitAfter(text, "Apartment ")[1]
			case "details":
				// 2 bedrooms • 1 bath • 920 sqft
				parts := strings.Split(text, " • ")

				for i, part := range parts {
					parts[i] = strings.TrimSpace(strings.SplitAfter(part, " ")[0])
				}

				//bed, e := strconv.Atoi(strings.SplitAfter(parts[0], " ")[0])
				bed, _ := strconv.Atoi(parts[0])
				bath, _ := strconv.Atoi(parts[1])
				sqft, _ := strconv.Atoi(parts[2])

				apt.Floorplan = Floorplan{
					Bedrooms:   bed,
					Bathrooms:  bath,
					Squarefeet: sqft,
					Loft:       false,
					ImageLink:  floorplanImage,
				}

			case "price":
				// nested span with price, lease information not in span
				re := regexp.MustCompile(`(\d\d?) mo`)
				apt.Price = selection.Find("span").First().Text()
				leaseterm := re.FindAllStringSubmatch(text, -1)[0]
				// if a match was found, it's the 1st element in the 0th return
				apt.LeaseTerm, _ = strconv.Atoi(leaseterm[1])

			case "availability":
				// available Dec 07 - Dec 08
				apt.Availability = strings.SplitAfter(text, "Available ")[1]
			}
		}

	})

	return apt, err
}

func (f Floorplan) Dump() {
	var fpType = reflect.ValueOf(&f).Elem()

	for i := 0; i < fpType.NumField(); i++ {
		field := fpType.Type().Field(i).Name
		val := fpType.Field(i)
		fmt.Printf("%s: %s\n", field, val)
	}
}
func (a Apartment) Dump() {
	var aptType = reflect.ValueOf(&a).Elem()

	for i := 0; i < aptType.NumField(); i++ {
		field := aptType.Type().Field(i).Name
		val := aptType.Field(i)
		if field == "Floorplan" {
			aptType.Field(i).MethodByName("Dump").Call([]reflect.Value{})
		} else {
			fmt.Printf("%s: %s\n", field, val)
		}
	}
}

func scrape() ([]Apartment, error) {

	var err error
	var ua = fmt.Sprintf("%s/%s", "shrug.pw", Random())

	var apartments []Apartment

	c := colly.NewCollector(colly.UserAgent(ua))

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.OnScraped(func(r *colly.Response) {
		fmt.Println("Finished", r.Request.URL)
	})

	c.OnHTML("ul.apartment-cards.available li.apartment-card", func(e *colly.HTMLElement) {
		var apartment, errA = ParseApartment(e)
		if errA != nil {
			err = errA
			return
		}
		apartments = append(apartments, apartment)
	})

	err = c.Visit(fmt.Sprintf("%s%s", ListingURL, TwoBeds))

	c.Wait()

	if err != nil {
		log.Fatal(err)
	}
	return apartments, err
}

func main() {
	var apartments, err = scrape()

	if err != nil {
		log.Fatal(err)
	}

	for _, apt := range apartments {
		apt.Dump()
		fmt.Println()
	}
}
